Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2016-02-01T20:36:17+00:00

====== Introduction to Composable Nanostates ======

There are many debates about good and best approaches to UI architecture and frameworks. This article introduces “composable nanostates” - the basic building block for UI applications.

====== UI Complexity Problem ======
UI complexity has grown tremendously over the last years. There are many screens with different types based on the asynchronously updating data. User may open several dialog windows displaying different projections of the same information and all the windows have to be updated simultaneously when data changes.

On each of those screens there are many different controls. And each control have its own state. This field is not valid and should have red background. Another one is disabled. And so on.

And this is not the only complexity. The state changes over time. User press a button and some fields are disabled. A request is fired to the server and some fields are populated when data arrives. User may do actions in different order. In popular applications those millions of users //will// generate different sequences of inputs. A single failure to update a dependent state could cause terrible results like field not being enabled after a button was present. Those "//specific scenario//" (thanks our QA team for the term) are very hard to debug and even harder to fix. Each fix have a chance to introduce another "specific scenario" of failure.

====== Single Source of Truth ======
If you look around, you will find that UI is not the first area hit by the Complexity Problem. The very similar problem exist everywhere where there is large amount of data. And it was natural that Databases had to solve the issue. 

Databases are designed to store huge amounts of data. However, not all the data are equal. Some elements of data are //plain facts//, like a banking transaction amount. They just store some fact. However, other pieces of information are //derived //from the facts. Account balance is just a total across all the transactions for the account.

The solution was a strong advise against storing the derived data when possible. Some databases provide materialized views for the basic data. Those views store computed data to provide good query performance but the derived data is also updated by the database engine when source data changes. In other databases you have to use triggers to achieve the same goal. However, the strong advise is the same: **"store only the base data but not the derived one"**.

If we could use the same advise, it could make our UI development simpler. If we could make our "derived state" (like background colour, field "enabled" status, etc...) update automatically when data changes, it will completely get rid of //specific scenario// type of failures. The same state of the data will provide the same UI output regardless of the way user get to that state. This also better for the user. They could completely rely on what they see, there is no //hidden state// caused by an order of their action.

===== One-Way Data Flow =====
Due to definition, "derived data" is derived from some other bits of data. It would not be natural to somehow mutate the derived data (it would not be derived any more). This creates one-way data flow. The data propagates from "primitive" values into derived values. Those "derived value" could serve as inputs to other "derived values" and so on.

Modern UI frameworks like Flux, Redux and React are adopting this approach. However, many of them have the same problem covered in the next chapter.

====== Single Point (of Failure) ======
Both Flux and React have a single global "entry point". This is a dispatcher in Flux or the store in Redux. The single point is detrimental for the composability.

There are many "screen-local" interactions in modern UIs. Should a "search" button be enabled on the search screen? In most cases, this decision is completely local to the screen (some fields should be filled and valid). However you //must// use the single point to change your model. Why can't the screen update its local state directly in Flux? Why should I ask somebody (dispatcher) to update my data? Why should the dispatcher care about my private data? What if there are many search screens open? How would the dispatcher find my specific data to update? Why should I provide data ID if I have my data just before me? The very similar set of questions applies to Redux (but instead of identifying the store you will identify a place in the global data tree).

All of that routing sound unnatural. Yes, these frameworks reduce a complexity of "derived state" management. However, they introduce a new problem of routing and composition. I like the perfect world so I do not like turning one problems into other.

====== Nanostates ======
Could we find another approach to managing the derived state? Could we make it without introducing any global objects? Well...

The solution is floating on the surface. We already have:
* Basic (mutable) state elements - the source of truth
* Derived data - created from other data elements based on some rules

What if we could represent each tiny (basic) element as a separate object on its own right? We should be able to mutate basic elements in some way. We also want to define rules to derive new "data elements". We could hide our own internal state from outside and we could manipulate it directly (no need for "update routing"). We create one-way data flow, so we also have all the benefits associated with it (predictability, ease of testing).

===== Programming API =====
I use Javascript notation, but this could be done on many languages with slightly different APIs.

As we already saw, we need few things. The first one is to **create a basic state element**:
''var state = R.variable("ABC")''
This creates a new "nanostate" with the value ABC.

We also need to **derive values based on other states:**
''var state1 = R.variable("DEF")''
''function fn(s1, s2, mult) {''
''  return (s1.length + s2.lenght) * mult;''
''}''
''var derived = R.apply(fn, state, state1, 5);''
Here we create a new value based on values of state and state1. This example also illustrates that values could be used to derive values, not only stores.

Sometimes we will need to **get a value stored in the state:**
''console.log(derived.value); // 30''
''console.log(state.value); // ABC''

If we **update** a basic value, all the derived values will change:
''state.set("XXXXX");''
''console.log(derived.value); // 40''

The key point here is that **all the derived values updated immediately after the change**. You are not required to "pull" the value or do anything else. So
''R.apply(console.log, derived); // prints 40 at the time of invocation''
''state1.set("BA"); // prints 35 as update is propagated to console.log "applied" on the previous step''
''state1.set("AB"); //prints nothing''
The last line illustrates another interesting point. Change **does not ** propagate through non-changed elements (i.e. it does not propagate to ''console.log''  through ''derived'' value as its value is still 35).

This covers basics of the API. But it is also the most common API you will use.

===== Consequences to Virtual DOM =====
The proposed model allows us a very interesting trick. Because change propagation is very precise, we **do not need** a virtual DOM or any other "rendering engine" (like react.js). With a small set of helpers, we could manipulate on the DOM attributes (or any other "native" attributes provided by the underlying platform) directly:
''function setClass(domElt, style) { // UI helper''
''  domElt.setAttribute("class", style);''
''}''
''val myElt = document.createElement("div");''
''val myStyle = R.apply(getButtonStyle, v1, v2, v3);''
''R.apply(setClass, myElt, myStyle);''

The binding is easy to implement. Any developer aware of the underlying platform could do that. They do not have to learn another API. Developers could add listeners directly to elements and events into the model actions.

And that easy of configuration is not just the theory. I use exactly the same Scala library for both Swing (desktop) and Vaading (Server-side stateful) applications. No modifications was done to the framework and bindings to UI library are pretty straightforward.

====== In the Wild ======
I have implemented this nanostates approach on different platforms for different applications. It works pretty well and exactly the same concept is used across all the different platforms. API bindings are slightly different, but this is caused by language difference, concepts are the same.
The languages it was implemented are:
* Actionscript (private project)
* Scala. The sample app with the library could be found here: https://github.com/maxkar/reactive-fx-scala-example
* JS port is on its way.
